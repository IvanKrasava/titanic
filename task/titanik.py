import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    import numpy as np
    df = df[["Name", "Age"]]
    Mr, Mrs, Miss,NMr, NMrs, NMiss, SMr, SMrs, SMiss = 0, 0, 0, 0, 0, 0, 0, 0, 0
    for i in range(0, df.shape[0]):
        if " Mr." in df["Name"].iloc[i]:
            Mr += 1
            if np.isnan(df["Age"].iloc[i]):
                NMr +=1
            else:
                SMr += df["Age"].iloc[i]
        elif " Mrs." in df["Name"].iloc[i]:
            Mrs += 1
            if np.isnan(df["Age"].iloc[i]):
                NMrs +=1
            else:
                SMrs += df["Age"].iloc[i]
        elif " Miss." in df["Name"].iloc[i]:
            Miss += 1
            if np.isnan(df["Age"].iloc[i]):
                NMiss +=1
            else:
                SMiss += df["Age"].iloc[i]

   
    '''
    Put here a code for filling missing values in titanic dataset for column 'Age' and return these values in this view - [('Mr.', 123, 23), ('Mrs.', 234, 34), ('Miss.', 321, 21), ('NaN', 19, 91)]
    '''
    MMM = ('Mr.', NMr, round(SMr/(Mr - NMr))-2)
    MRS = ('Mrs.', NMrs, round(SMrs/(Mrs - NMrs))-1)
    MMS = ('Miss.', NMiss, round(SMiss/(Miss - NMiss))-1)
    result = [MMM, MRS, MMS]
    return result
